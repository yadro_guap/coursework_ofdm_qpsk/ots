#include <complex>
#include <iostream>
#include <valarray>
#define _USE_MATH_DEFINES
#include <math.h>
#include <vector>

typedef std::complex <double> Complex;
typedef std::valarray <Complex> CArray;

void fft(CArray& x) {
	const size_t N = x.size();
	if (N <= 1) return;
	CArray even = x[std::slice(0, N / 2, 2)];
	CArray  odd = x[std::slice(1, N / 2, 2)];
	fft(even);
	fft(odd);
	for (size_t k = 0; k < N / 2; ++k) {
		Complex t = std::polar(1.0, -2 * M_PI * k / N) * odd[k];
		x[k] = even[k] + t;
		x[k + N / 2] = even[k] - t;
	}
}

void ifft(CArray& x) {
	x = x.apply(std::conj);
	fft(x);
	x = x.apply(std::conj);
	x /= x.size();

}

std::string charToBinary(char c) {
    std::string result;
    for (int i = 7; i >= 0; --i) {
        result += ((c & (1 << i)) ? '1' : '0');
    }
    return result;
}

int main() {
    
   //std::string message;

    //std::cout << "Input message: ";
    //std::getline(std::cin, message);
    //std::string binary;
    //for (char c : message) {
      //  binary = charToBinary(c);
        //std::cout << binary;
    //}

    
    std::string message = "Hello";
    const int numChars = message.size();
    const int numBits = 8;


    std::vector<Complex> test;
    for (int i = 0; i < numChars; ++i) {
        std::string binary = charToBinary(message[i]);
        for (int j = 0; j < numBits; ++j) {
            test.push_back(Complex(binary[j] - '0', 0));
        }
    }

    CArray data(test.data(), test.size());
    fft(data);

    std::cout << "fft" << std::endl;
    for (int i = 0; i < numChars * numBits; ++i) {
        std::cout << data[i] << std::endl;
    }

    ifft(data);

    std::cout << std::endl << "ifft" << std::endl;
    for (int i = 0; i < numChars * numBits; ++i) {
        std::cout << data[i] << std::endl;
    }

    return 0;
}