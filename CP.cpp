#include <iostream>
#include <vector>
#include <complex>

using namespace std;

class CPAdder {
public:
    static vector<complex<double>> addCP(const vector<complex<double>>& symbols, size_t cpLength) {
        vector<complex<double>> withCP;
        withCP.reserve(symbols.size() + cpLength);

        // Add cyclic prefix
        for (size_t i = symbols.size() - cpLength; i < symbols.size(); ++i) {
            withCP.push_back(symbols[i]);
        }

        // Add symbols
        for (const auto& symbol : symbols) {
            withCP.push_back(symbol);
        }

        return withCP;
    }
};
