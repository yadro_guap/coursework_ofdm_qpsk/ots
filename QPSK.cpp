#include <iostream>
#include <cmath>
#include <complex>
#include <vector>

using namespace std;

class QPSKModulator {
public:
    static vector<complex<double>> modulate(const vector<int>& data) {
        vector<complex<double>> symbols;
        symbols.reserve(data.size() / 2);
        for (size_t i = 0; i < data.size(); i += 2) {
            int I = data[i];
            int Q = data[i + 1];
            double phase = 3.14 / 4 * (2 * I + Q);
            symbols.emplace_back(cos(phase), sin(phase));
        }
        return symbols;
    }
};

class QPSKDemodulator {
public:
    static vector<int> demodulate(const vector<complex<double>>& symbols) {
        vector<int> data;
        data.reserve(symbols.size() * 2);
        for (const auto& symbol : symbols) {
            double phase = std::atan2(symbol.imag(), symbol.real());
            int I = static_cast<int>(round(cos(phase) / sqrt(2)));
            int Q = static_cast<int>(round(sin(phase) / sqrt(2)));
            data.push_back(I);
            data.push_back(Q);
        }
        return data;
    }
};
