#include <complex>
#include <vector>
#include <cmath>

using namespace std;

class FFT {
public:
    static void transform(vector<complex<double>>& a, bool inverse = false) {
        int n = a.size();
        if (n <= 1) return;

        vector<complex<double>> even, odd;
        even.reserve(n / 2);
        odd.reserve(n / 2);
        for (int i = 0; i < n; i += 2) {
            even.push_back(a[i]);
            odd.push_back(a[i + 1]);
        }

        transform(even, inverse);
        transform(odd, inverse);

        // сортировка
    }

    static void fft(vector<complex<double>>& a) {
        transform(a, false);
    }
};

class IFFT {
public:
    static void transform(vector<complex<double>>& a) {
        FFT::transform(a, true);
    }

    static void ifft(vector<complex<double>>& a) {
        transform(a);
    }
};



