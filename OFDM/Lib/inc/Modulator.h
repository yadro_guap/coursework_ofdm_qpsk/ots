/* Modulator.h */

#pragma once

#include "Base.h"
#include "Array.h"
#include "QPSK.h"

namespace ofdm {

class Modulator
 {
   OFDMConfig cfg;

   Array<Sample> codepoints;
   QPSKCoder qpsk;

   // TODO

  private:

   Modulator(const Modulator &) = delete;

   Modulator & operator = (const Modulator &) = delete;

  private:

   void codeData(const octet *data);

   void placeCodepoints(Sample *body);

   void doFFT(Sample *body);

   void addCP(Sample *symbol);

  public:

   explicit Modulator(const OFDMConfig &cfg);

   ~Modulator();

   const OFDMConfig & getCfg() const { return cfg; }

   void genSymbol(const octet *data,Sample *symbol);
 };

} // namespace ofdm
