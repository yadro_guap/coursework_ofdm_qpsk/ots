/* Array.h */

#pragma once

#include "Base.h"

namespace ofdm {

template <class T>
class Array
 {
   T *ptr;
   size_t len;

  private:

   Array(const Array &) = delete;

   Array & operator = (const Array &) = delete;

  public:

   explicit Array(size_t len_)
    {
     ptr=new T[len_];
     len=len_;
    }

   ~Array()
    {
     delete[] ptr;
    }

   T * getPtr() { return ptr; }

   const T * getPtr() const { return ptr; }

   size_t getLen() const { return len; }
 };

} // namespace ofdm
