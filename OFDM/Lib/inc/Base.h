/* Base.h */

#pragma once

#include <stddef.h>
#include <stdint.h>

#include <complex>

namespace ofdm {

/* types */

using uint = unsigned int ;

using octet = uint8_t ;
using Sample = std::complex<double> ;

/* functions */

inline constexpr uint Pow2(uint d) { return 1u<<d; }

void Guard(const char *msg);

inline void Assert(bool cond,const char *msg) { if( !cond ) Guard(msg); }

/* classes */

struct OFDMConfig
 {
  uint orderFFT;
  uint lenFFT;
  uint lenCP;
  uint lenSymbol;

  uint lenBW;
  uint lenBits;
  uint lenOctets;

  explicit OFDMConfig(uint orderFFT);
 };

} // namespace ofdm
