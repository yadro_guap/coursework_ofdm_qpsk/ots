/* QPSK.h */

#pragma once

#include "Base.h"

namespace ofdm {

class QPSKCoder
 {
   // TODO

  public:

   QPSKCoder();

   ~QPSKCoder();

   void codeData(const octet *data,uint lenOctets,Sample *out);
 };

} // namespace ofdm
