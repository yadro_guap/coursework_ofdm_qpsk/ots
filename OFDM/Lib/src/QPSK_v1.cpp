/* QPSK.cpp */

#include "QPSK.h"
#define _USE_MATH_DEFINES
#include <cmath>

namespace ofdm {

void QPSKCoder::codeData(octet data,Sample *out)
 {
  // 8 bit   b7 b6 ... b0
  // ( b7 b6 ) ( b5 b4) ( b3 b2 ) ( b1 b0 )

  unsigned p0= (data >> 6) & 3u;
  unsigned p1= (data >> 4) & 3u;
  unsigned p2= (data >> 2) & 3u;
  unsigned p3= (data) & 3u;

  // 0,1,2,3 -> Sample
  static const Sample qpskMap[4] = { (1 / sqrt2) + (1 / sqrt2) * 1j, (1 / sqrt2) - (1 / sqrt2) * 1j, -(1 / sqrt2) + (1 / sqrt2) * 1j, -(1 / sqrt2) - (1 / sqrt2) * 1j };
  

  out[0] = qpskMap[p0];
  out[1] = qpskMap[p1];
  out[2] = qpskMap[p2];
  out[3] = qpskMap[p3];
 }

QPSKCoder::QPSKCoder()
 {
 }

QPSKCoder::~QPSKCoder()
 {
 }

void QPSKCoder::codeData(const octet *data,uint lenOctets,Sample *out)
 {
  for(uint i=0; i<lenOctets ;i++)
    {
     codeData(data[i],out);

     out+=4;
    }
 }

} // namespace ofdm
