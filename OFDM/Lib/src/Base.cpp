/* Base.cpp */

#include "Base.h"

#include <stdexcept>

namespace ofdm {

/* functions */

void Guard(const char *msg)
 {
  throw std::runtime_error(msg);
 }

/* classes */

OFDMConfig::OFDMConfig(uint orderFFT_)
 {
  Assert( orderFFT_>=4 ,"orderFFT is too small");
  Assert( orderFFT_<=16 ,"orderFFT is too big");

  orderFFT=orderFFT_;
  lenFFT=Pow2(orderFFT);

  lenCP=lenFFT/16; // predefined
  lenBW=3*(lenFFT/8); // predefined

  Assert( lenCP>0 ,"lenCP is too small");
  Assert( lenCP<lenFFT ,"lenCP is too big");

  lenSymbol=lenFFT+lenCP;

  Assert( lenBW>0 ,"lenBW is too small");
  Assert( lenBW<lenFFT/2 ,"lenBW is too big");

  Assert( (lenBW%2)==0 ,"lenBW must be even");

  lenBits=4*lenBW; // 2 bits per codepoint in QPSK, 2*lenBW codepoints
  lenOctets=lenBits/8;
 }

} // namespace ofdm
