/* main.cpp */

#include "test.h"

#include <exception>
#include <iostream>

int main()
 {
  using namespace ofdm;

  try
    {
     testOFDMConfig();
    }
  catch(std::exception &ex)
    {
     std::cout << "\nException: " << ex.what() << std::endl ;

     return 1;
    }
  catch(...)
    {
     std::cout << "\nException: unknown exception" << std::endl ;

     return 1;
    }

  std::cout << "\nSuccess" << std::endl ;

  return 0;
 }
