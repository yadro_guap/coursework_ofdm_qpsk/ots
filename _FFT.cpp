// FFT.cpp 

#include "FFT.h"

namespace ofdm {

void FFT::batterfly(Sample *data, size_t dataLen) const // ���������� ������� � ���������� data
 {
	for (size_t i = 0; i < dataLen / 2; i++) 
	{
		Sample even = data[i];
		Sample odd = data[i + dataLen / 2];

		data[i] = even + odd;
		data[i + dataLen / 2] = even - odd;
	}
 }

void FFT::multiply(Sample *data, size_t dataLen, size_t div) const 
 {

 }

void FFT::permutate(Sample *data, size_t dataLen) const
 {
	std::vector<Sample> temp(dataLen); 
	size_t evenIndex = 0;
	size_t oddIndex = dataLen / 2;

	for (size_t i = 0; i < dataLen; ++i) 
	{
		if (i % 2 == 0) 
		{
			temp[evenIndex++] = data[i];
		}
		else 
		{
			temp[oddIndex++] = data[i];
		}
	}
	std::copy(temp.begin(), temp.end(), data);
 }

void FFT::fft(Sample *data, size_t dataLen, size_t div) const
 {
	  batterfly(data,dataLen);

	  if( dataLen==2 ) return;

	  fft(data,dataLen/2,2div);

	  multiply(data+dataLen/2,dataLen/2,2div);

	  fft(data+dataLen/2,dataLen/2,2div);

	  permutate(data,dataLen);
 }

FFT::FFT(size_t len,bool direct)//��� ������ ������� ������, ������� ��� ����� ����� �������� ��� ������, ���� ��������� �������� ������
 : roots(len/2),
   len(len)
 {
  Assert( len_>=2 ,"FFT length is too small");

  // TODO fill roots
 }

FFT::~FFT()
 {
 }

void FFT::operator () (Sample *data) const
 {
  fft(data,len,1);
 }

} // namespace ofdm