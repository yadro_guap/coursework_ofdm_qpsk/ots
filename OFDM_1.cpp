#include <iostream>
#include <complex>
#include <vector>
#include <cmath>
#include <string>
#include <valarray>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;

typedef complex <double> Complex;
typedef valarray <Complex> CArray;
const int M = 2048;

string charToBinary(char c) {
    string result;
    for (int i = 7; i >= 0; --i) {
        result += ((c & (1 << i)) ? '1' : '0');
    }
    return result;
}

vector<complex<double>> qpsk_modulate(const vector<int>& data) {
    vector<complex<double>> symbols;
    for (size_t i = 0; i < data.size(); i += 2) {
        int I = data[i];
        int Q = data[i + 1];

        double phase = M_PI / 4 * (2 * I + Q);
        complex<double> symbol(cos(phase), sin(phase));
        symbols.push_back(symbol);
    }
    return symbols;
}

void fft(CArray& x) {
    const size_t N = x.size();
    if (N <= 1) return;
    CArray even = x[slice(0, N / 2, 2)];
    CArray  odd = x[slice(1, N / 2, 2)];
    fft(even);
    fft(odd);
    for (size_t k = 0; k < N / 2; ++k) {
        Complex t = polar(1.0, -2 * M_PI * k / N) * odd[k];
        x[k] = even[k] + t;
        x[k + N / 2] = even[k] - t;
    }
}

void ifft(CArray& x) {
    x = x.apply(conj);
    fft(x);
    x = x.apply(conj);
    x /= x.size();
}


int main() {
    string message;
    vector<int> binary;
    string bin;

    //���� ���������
    cout << "Input message: ";
    getline(cin, message);

    //������� � �������� �������
    for (char c : message) bin += charToBinary(c);

    for (int i = 0; i < bin.size(); i++) binary.push_back(int(bin[i]) - 48);

    cout << endl << "Binary:" << endl;
    for (auto& x : binary) cout << x;

    //������� � ����������� �����
    vector<complex<double>> symbols = qpsk_modulate(binary);

    cout << endl << endl << "Complex:" << endl;
    for (const auto& symbol : symbols) cout << symbol.real() << " + " << symbol.imag() << "i" << endl;

    //�������� ���
    CArray data(symbols.data(), symbols.size());
    ifft(data);

    cout << endl << "IFFT:" << endl;
    for (auto& x : data) cout << x << endl;

    return 0;
}
