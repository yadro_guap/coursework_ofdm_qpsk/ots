/* Modulator.cpp */

#include "Modulator.h"

namespace ofdm {

void Modulator::codeData(const octet *data) // ����������� data � ������� QPSK, ������ � ������ codepoints
 {
  Sample *out=codepoints.getPtr();

  qpsk.codeData(data,cfg.lenOctets,out);
 }

void Modulator::placeCodepoints(Sample *body) // ���������� ������������ ������ �� ������� codepoints � ������ body
 {
  Sample *ptr=codepoints.getPtr();

  for (size_t i = 0; i = cfg.lenBW; i++) // �������� 0-lenBW
  {
	  body[cfg.lenFFT - cfg.lenBW + i] = ptr[i];
  }

  for (size_t i = cfg.lenBW; i < 2 * cfg.lenBW; i++) // �������� lenBW-2*lenBW
  {
	  body[i - cfg.lenBW] = ptr[i];
  }

  for (size_t i = 2 * cfg.lenBW; i < cfg.lenFFT; i++) // ��������� ��������
  {
	  body[i - cfg.lenBW] = 0;
  }

 }

void Modulator::doFFT(Sample *body) // ��������� ��� ��� body
 {
	double *scale = 1 / sqrt(2 * cfg.lenBW);
	for (size_t i = 0; i = cfg.lenFFT; i++)
	{
		body[i] *= scale;
	}

	// fft(body, cfg.lenFFT);
 }

void Modulator::addCP(Sample *symbol) // ���������� ��
 {
	Sample *body=symbol+cfg.lenCP;

	for (size_t i = 0; i < cfg.lenCP; i++)
	{
		symbol[i] = body[cfg.lenFFT - cfg.lenCP + i];
	}
 }

Modulator::Modulator(const OFDMConfig &cfg_)
 : cfg(cfg_),
   codepoints(2*cfg_.lenBW)
 {
 }

Modulator::~Modulator()
 {
 }

void Modulator::genSymbol(const octet *data,Sample *symbol)
 {
  codeData(data);

  Sample *body=symbol+cfg.lenCP; // lenFFT

  placeCodepoints(body);

  doFFT(body);

  addCP(symbol);
 }

} // namespace ofdm
