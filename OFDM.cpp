#include <iostream>
#include <complex>
#include <vector>
#include <cmath>
#include <string>
#include <valarray>
#define _USE_MATH_DEFINES
#include <math.h>

const int M = 2048;

// ��������� � 01010101 � ��
std::string charToBinary(char c) {
    std::string result;
    for (int i = 7; i >= 0; --i) {
        result += ((c & (1 << i)) ? '1' : '0');
    }
    return result;
}


//��������� ����� ��������
std::vector<std::complex<double>> qpsk_modulate(const std::vector<int>& data) {
    std::vector<std::complex<double>> symbols;
    for (size_t i = 0; i < data.size(); i+=2) {
        int I = data[i];
        int Q = data[i + 1];

        double phase = 3.14 / 4 * (2 * I + Q);
        std::complex<double> symbol(cos(phase), sin(phase));
        symbols.push_back(symbol);
    }
    return symbols;
}


// �����
typedef std::complex <double> Complex;
typedef std::valarray <Complex> CArray;

void fft(CArray& x) {
    const size_t N = x.size();
    if (N <= 1) return;
    CArray even = x[std::slice(0, N / 2, 2)];
    CArray  odd = x[std::slice(1, N / 2, 2)];
    fft(even);
    fft(odd);
    for (size_t k = 0; k < N / 2; ++k) {
        Complex t = std::polar(1.0, -2 * M_PI * k / N) * odd[k];
        x[k] = even[k] + t;
        x[k + N / 2] = even[k] - t;
    }
}

void ifft(CArray& x) {
    x = x.apply(std::conj);
    fft(x);
    x = x.apply(std::conj);
    x /= x.size();
}



/*
// ��������� ������
std::vector<std::complex<double>> generateData() {
    std::vector<std::complex<double>> data(M);
    for (int i = 0; i < M; ++i) {
        data[i] = { rand() % 100 / 100.0, rand() % 100 / 100.0 };
    }
    return data;
}


// �������� �������������� �����
std::vector<std::complex<double>> inverseFFT(const std::vector<std::complex<double>>& data) {
    std::vector<std::complex<double>> result(M);
    // ��������� �������� �������������� �����
    return result;
}



// ���������� ���������
std::vector<std::complex<double>> impulseModulation(const std::vector<std::complex<double>>& data) {
    std::vector<std::complex<double>> modulatedData(M);
    // ��������� ���������� ���������
    return modulatedData;
}

// ����������
std::vector<std::complex<double>> filter(const std::vector<std::complex<double>>& data) {
    std::vector<std::complex<double>> filteredData(M);
    // ��������� ����������
    return filteredData;
}

// �����-���������� �������������� (���)
std::vector<double> DAC(const std::vector<std::complex<double>>& data) {
    std::vector<double> analogSignal(M);
    // ��������� ���
    return analogSignal;
}

// ��������� �� ������� �������
std::vector<std::complex<double>> modulateCarrier(const std::vector<double>& analogSignal, double fc) {
    std::vector<std::complex<double>> modulatedSignal(M);
    // ��������� ��������� �� ������� �������
    return modulatedSignal;
}

*/

int main() {

    std::vector<int> data = { 0, 0, 1, 0, 1, 1, 0, 1 };
    std::vector<std::complex<double>> symbols = qpsk_modulate(data);

    for (const auto& symbol : symbols) {
        std::cout << symbol.real() << " + " << symbol.imag() << "i" << std::endl;
    }

    std::string message;

    std::cout << "Input message: ";
    std::getline(std::cin, message);
    std::string binary;
    for (char c : message) {
        binary = charToBinary(c);
        std::cout << binary;
    }


    const double Tu = 224e-6;
    const double T = Tu / 2048;
    const double G = 1 / 4;
    const double delta = G * Tu;
    const double Ts = delta + Tu;
    const int Kmax = 1705;
    const int FS = 4096;
    const int q = 10;
    const double fc = q * 1 / T;
    const double Rs = 4 * fc;









    /*
    // ��������� ������
    std::vector<std::complex<double>> data = generateData();

    // �������� �������������� �����
    std::vector<std::complex<double>> carriers = inverseFFT(data);

    // ���������� ���������
    //std::vector<std::complex<double>> modulatedData = impulseModulation(carriers);

    // ����������
    //std::vector<std::complex<double>> filteredData = filter(modulatedData);

    // �����-���������� �������������� (���)
    //std::vector<double> analogSignal = DAC(filteredData);

    // ��������� �� ������� �������
    //std::vector<std::complex<double>> modulatedSignal = modulateCarrier(analogSignal, fc);

    // ����� ���������� ��� ���������� ��������
    */

    return 0;
}
